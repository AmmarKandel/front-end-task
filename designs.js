// Select color input
// Select size input

// When size is submitted by the user, call makeGrid()


var submitBt = document.getElementById("submit-bt");
submitBt.addEventListener("click", function makeGrid(e){
    //prevent the default of submit input to stop refresh the page
    e.preventDefault();
    let theTable = document.getElementById("pixelCanvas"),
        inputHeight = document.getElementById("inputHeight").value,
        inputWidth = document.getElementById("inputWidth").value,
        tableContent = "";
    //loop to add <tr> Based on the user input height value
    for(let A = 0; A < inputHeight; A++){
        tableContent+="<tr>";
        //loop to add <td> Based on the user input width value 
        for(let y = 0; y < inputWidth; y++){
             tableContent+="<td></td>";
        }
        tableContent+="</tr>"
    }
    //add the the tableContent to the <table></table>
    theTable.innerHTML=tableContent;

    function colorFunc(){
        let allTd = document.getElementsByTagName("td");

        //add input color value for every td clicked
        for(let z = 0; z < allTd.length; z++){
             allTd[z].addEventListener("click", function(e){
                 let specificTd = e.target,
                     colorPicker = document.getElementById("colorPicker").value;

                 specificTd.style.backgroundColor = colorPicker;
             })
        }
    }
    colorFunc();
})